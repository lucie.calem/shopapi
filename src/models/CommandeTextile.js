import {Schema, model } from 'mongoose';

const CommandeSchema = new Schema ({
    id_article: { type: String, required: true },
    id_user: { type: String, required: true },
    category: {
        type: Schema.Types.ObjectId, ref: 'Category'
    },
    libelle: { type: String, required: true },
    image: { type: String,required: true },
    marque: { type: String, required: true },
    status: { type: String, required: true },
    prix:  {type: Number, required: true}, 
    created_at: { type: Date, default: Date.now() },
})

const CommandeTextileSchema = new Schema({
    ...CommandeSchema.obj,
    taille:{type: String, default: null},
    elasthanne:  {type: Boolean, required: true},
    lavage: {type: String, required: true},
    
});
  
export default model('CommandeTextile', CommandeTextileSchema);
