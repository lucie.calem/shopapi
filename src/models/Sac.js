import {Schema, model } from 'mongoose';

const ArticleSchema = new Schema ({
    category: {
        type: Schema.Types.ObjectId, ref: 'Category'
    },
    libelle: { type: String, required: true },
    image: { type: String, required: true },
    marque: { type: String, required: true },
    prix:  {type: Number, required: true}, 
    stock: {type: Number, default: null},
    created_at: { type: Date, default: Date.now() },
})
const SacSchema = new Schema({
    ...ArticleSchema.obj,
    contenance: {type: Number, required: true},
    anse:  {type: Boolean, required: true},
    refermable:  {type: Boolean, required: true}, 
    
});

  
export default model('Sac', SacSchema);
