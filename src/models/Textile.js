import {Schema, model } from 'mongoose';

const ArticleSchema = new Schema ({
    category: {
        type: Schema.Types.ObjectId, ref: 'Category'
    },
    libelle: { type: String, required: true },
    image: { type: String, required: true },
    marque: { type: String, required: true },
    prix:  {type: Number, required: true}, 
    stock: {type: String, default: null},
    created_at: { type: Date, default: Date.now() },
})
const TextileSchema = new Schema({
    ...ArticleSchema.obj,
    taille:{type: String, default: null},
    elasthanne:  {type: Boolean, required: true},
    lavage: {type: String, required: true},
    
});

export default model('Textile', TextileSchema);