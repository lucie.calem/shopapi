import {Schema, model } from 'mongoose';

const UserSchema = new Schema ({
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    mail: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    role: { type: Number, default: 0 },
    phone:  {type: Number, default: null}, 
    address:{type: String, default: null},
    zip_code: {type: String, required: true},
    created_at: { type: Date, default: Date.now() },
})

export default model('User', UserSchema);