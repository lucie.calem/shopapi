import { Router } from 'express';
import UserController from './controllers/UserController';
import CategoryController from './controllers/CategoryController';
import Auth from './utils/Auth';
import ArticleController from './controllers/ArticleController';
import CommandeController from './controllers/CommandeController';
import Multer from './utils/Multer';

const router = Router();

/**
 * 
 * Users C.R.U.D : 
 * 
 * add middle ware sur le role de l'utilisateur
 * 
 */
router.post('/users/', UserController.store); //creation d'un user
router.get('/users/', UserController.list); //liste des users pour les admin 
router.get('/users/:id', UserController.details); //detail d'un user 
router.put('/users/:id', UserController.update); //maj d'un user 
router.delete('/users/:id', UserController.remove); //suppression d'un user 
router.post('/users/auth', UserController.auth); //auth + generation token

/**
 * 
 * Articles C.R.U.D : 
 * 
 * add middle ware sur le role de l'utilisateur pour la gestion des article 
 * rajouter une fois fini Auth.isAllowed([10])
 */
router.post('/articles/:typeArticle/',Multer.upload('articles','image'),ArticleController.store); //creation d'un article

router.get('/articles/:typeArticle/',ArticleController.list); //liste des article pour les admin 
router.get('/articles/:typeArticle/:id', ArticleController.details); //detail d'un article 
router.put('/articles/:typeArticle/:id', Auth.isAllowed([10]), ArticleController.update); //maj d'un article 
router.put('/articles/:typeArticle/:id/image',Multer.upload('articles','image'), ArticleController.updateImage);// maj de l'image d'un article 
router.delete('/articles/:typeArticle/:id', Auth.isAllowed([10]), ArticleController.remove); //suppression d'un article 

/**
 * 
 * Category C.R.U.D : 
 * add middle ware sur le role de l'utilisateur pour la gestion des catégories
 * rajouter une fois fini Auth.isAllowed([10])
 */
router.post('/categories/', Auth.isAllowed([10]), CategoryController.store); //creation d'une category
router.get('/categories/', CategoryController.list); //liste des categories
router.put('/categories/:id',Auth.isAllowed([10]), CategoryController.update); //maj d'une category
router.delete('/categories/:id',Auth.isAllowed([10]), CategoryController.remove); //suppression d'une category
/**
 * 
 * Panier/Commande C.R.D
 * 
 */
router.get('/articles/:typeArticle/listPanier/:id', CommandeController.listPanier); 
router.get('/articles/:typeArticle/listCommande/:id', CommandeController.listCommande); 
router.post('/articles/:typeArticle/add',CommandeController.add); 
router.put('/articles/:typeArticle/update/:id',CommandeController.update); 
router.put('/articles/:typeArticle/updateAll/:id',CommandeController.updateAll); 
router.delete('/articles/:typeArticle/deletePanier/:id/:id_user', CommandeController.deletePanier); 




export default router;