import dotenv from 'dotenv';
dotenv.config();
import expressJwt from 'express-jwt';
import User from '../models/User';

const jwt = () => {
    const secret = process.env.JWT_SECRET;
    return expressJwt({ secret: secret, algorithms: ['HS256'], isRevoked })
        .unless({
            path: [
                '/users/auth',
                '/articles/sac',
                '/articles/textile',
                '/categories',
                /^\/uploads\/.*/,
                /^\/api-docs\/.*/, //swagger
            ]
        });
};

async function isRevoked(req, playload, done) {
    const user = await User.findById(playload.sub);
    if (!user) {
        return done(null, true);
    }
    done();
}

export default jwt;