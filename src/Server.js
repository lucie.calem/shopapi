//Config serveur
//import des modules
import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import router from './routes';
import jwt from './config/jwt';
import swaggerUi from 'swagger-ui-express';
import swaggerJsDoc from 'swagger-jsdoc';
import yamljs from 'yamljs';
const swaggerDoc = yamljs.load('swagger.yaml');

export default class Server {

    //configuration serveur
    static config() {

        //init app avec express
        const app = express();

        //acces swagger 
        app.use('/api-docs',swaggerUi.serve, swaggerUi.setup(swaggerDoc));
        app.use('/uploads', express.static('uploads'));
        app.use(jwt()); //obligation de ce connecter

        //configuration de l'app
        app.use(bodyParser.urlencoded({ extended: false })); //traduit du json à l'intérieur des requetes 
        app.use(bodyParser.json()); // int et utilise le js
        app.use(cors({ origin: true })); //object avec des options, si API sur un serveur: accepte la connection à la web app

        //Configuration des routes de l'api depuis routes.js
        app.use('/', router);
        return app;
    }
}