import multer from 'multer';
import path from 'path';
import fs from 'fs'; //file system
import Utils from './utils'

export default class Multer {

    static upload(updloadPath, identifier) {
        return (req, res, next) => {
            const storage = multer.diskStorage({
                destination:(req,file,cb) => {
                    //spe le chemin et upload apres
                    if(!fs.existsSync(`./uploads`)){
                        fs.mkdirSync(`./uploads`);
                    }
                    if(!fs.existsSync(`./uploads/${updloadPath}`)){
                        fs.mkdirSync(`./uploads/${updloadPath}`);
                    }
                    cb(null,`./uploads/${updloadPath }`);
                },
                filename: (req,file,cb)=> {
                    let name = Utils.generateStringRandom() + path.extname(file.originalname);
                    req.body[identifier] = `./uploads/${updloadPath}/${name}`;  // identifer représente le nom du champ de l'image ( thumbnail dans ce cas si )
                    cb(null, name)
                }
            });

            let upl = multer({storage}).single(identifier);
            upl (req,res,next, () => {
                next();
            });
        }
    }


}