import { connect } from 'mongoose';

export default class Database {

    //init database
    static init(url, options = {}) {
        options = Object.assign({}, {
            useCreateIndex: true,
            useNewUrlParser: true,
            useUnifiedTopology: true,
        }, options);
        return connect(url, options);
    }
}