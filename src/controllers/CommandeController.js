import CommandeSac from '../models/CommandeSac';
import CommandeTextile from '../models/CommandeTextile';
export default class Commandecontroller {

   
   static async add(req, res) {
        let status = 200;
        let body = {};
        try {
            let {typeArticle} = req.params;
            let article = typeArticle == 'textile'?
            await CommandeTextile.create(req.body) : typeArticle == 'sac'?
            await CommandeSac.create(req.body) : '';
           body = {article};
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Add an article to the Panier',
                message: e.message || 'An error is occured into the article creation on panier',
            }
        }
        return res.status(status).json(body);
    }

    static async update(req, res) {
        let status = 200;
        let body = {};
  
        try {
            console.log(req.body.id_article);
            let {id} = req.params;
            let status = "en cours";
            let {typeArticle} = req.params;
            let article = typeArticle == 'textile'?
            await CommandeTextile.findOneAndUpdate({"id_user":id,"status":status,'id_article':req.body.id_article},{"status":"terminer"},{new:true}) : typeArticle == 'sac'?
            await CommandeSac.findOneAndUpdate({"id_user":id,"status":status,'id_article':req.body.id_article},{"status":"terminer"},{new:true}): '';
            console.log(article);
            body = {article};
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Add an article to the Commande',
                message: e.message || 'An error is occured into the article add on Commande',
            }
        }
        return res.status(status).json(body);
    }

    static async updateAll(req, res) {
        let status = 200;
        let body = {};
  
        try {
            
            let { id} = req.params;
            let status = "en cours";
            let {typeArticle} = req.params;
            let article = typeArticle == 'textile'?
            await CommandeTextile.updateMany({"id_user":id,"status":status},{"status":"terminer"}) : typeArticle == 'sac'?
            await CommandeSac.updateMany({"id_user":id,"status":status},{"status":"terminer"}): '';
          
            body = {article};
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Add an article to the Panier',
                message: e.message || 'An error is occured into the article add on panier',
            }
        }
        return res.status(status).json(body);
    }


//get list of articles
static async listPanier(req, res) {
    let status = 200;
    let body = {};

    try {
        let {typeArticle} = req.params;
        let { id } = req.params;
        let status = "en cours";
        let articles = typeArticle == 'textile'?
        await CommandeTextile.find({"id_user":id,"status":status}).select('-__v') : typeArticle == 'sac'?
        await CommandeSac.find({"id_user":id,"status":status}).select('-__v'): '';
        body = { articles };
    } catch (e) {
        status = status !== 200 ? status : 500;
        body = {
            error: e.error || 'List of Articles of panier',
            message: e.message || 'An error is occured into list of Articles of panier',
        }
    }
    return res.status(status).json(body);
}

    //get list of commande
    static async listCommande(req, res) {
        let status = 200;
        let body = {};

        try {
            let {typeArticle} = req.params;
            let { id } = req.params;
            let status = "terminer";
            let articles = typeArticle == 'textile'?
            await CommandeTextile.find({"id_user":id,"status":status}).select('-__v') : typeArticle == 'sac'?
            await CommandeSac.find({"id_user":id,"status":status}).select('-__v'): '';
            body = { articles };
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'List of Commands',
                message: e.message || 'An error is occured into list of Commands',
            }
        }
        return res.status(status).json(body);
    }


    static async deletePanier(req, res) {
        let status = 200;
        let body = {};

        try {
            let { id ,id_user} = req.params;
            let status = "en cours";
            let {typeArticle} = req.params;
            let article = typeArticle == 'textile'?
            await CommandeTextile.findOneAndDelete({"id_article":id,"id_user":id_user,"status":status}) : typeArticle == 'sac'?
            await CommandeSac.findOneAndDelete({"id_article":id,"id_user":id_user,"status":status}): '';
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Delete Articles from panier',
                message: e.message || 'An error is occured into Delete Articles from our panier',
            }
        }
        return res.status(status).json(body);
    }

    
}