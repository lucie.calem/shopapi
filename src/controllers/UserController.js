import User from '../models/User';
import jsonwebtoken from 'jsonwebtoken';

export default class ArticleControllerUserController {


    /**
     * create one user
     */
    static async store(req, res) {
        let status = 200;
        let body = {};

        try {
            let user = await User.create(req.body);
            body = { user };
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Create User',
                message: e.message || 'An error is occured into the creation',
            }
        }
        return res.status(status).json(body);
    }

    /**
    * List of users
    */
    static async list(req, res) {
        let status = 200;
        let body = {};

        try {
            let users = await User.find().select('-__v -password -phone -adress')
            body = { users };
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'List of Users',
                message: e.message || 'An error is occured into list of users',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Details of a user
     */
    static async details(req, res) {
        let status = 200;
        let body = {};

        try {
            let { id } = req.params;
            let user = await User.findById(id).select('-__v -password -created_at');
            body = { user };
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Details User',
                message: e.message || 'An error is occured into Details User',
            }
        }
        return res.status(status).json(body);
    }

    /**
    * update one user
    * 
    */
    static async update(req, res) {
        let status = 200;
        let body = {};

        try {
            let { id } = req.params;
            let user = await User.findByIdAndUpdate(id, req.body, { new: true }).select('-__v -password -created_at');
            body = { user };
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Update User',
                message: e.message || 'An error is occured into Update User',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * delete one user
     * 
     */

    static async remove(req, res) {
        let status = 200;
        let body = {};

        try {
            let { id } = req.params;
            let user = await User.findByIdAndDelete(id);
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Delete User',
                message: e.message || 'An error is occured into Delete User',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * auth one user
     * quand on fait le swagger : mail, password et JWT_SECRET
     * 
     * {
        "mail": "lucie@mail.com",
        "password": "lucie",
        "JWT_SECRET": "shop"
        }
     */
    static async auth(req, res) {
        let status = 200;
        let body = {};

        try {
            let { mail, password } = req.body;
            let user = await User.findOne({ 'mail': mail }).select('-__v');
            if (user && password === user.password) {
                //L'utilisateur existe dans la DB
                let { JWT_SECRET } = process.env;
                let token = jsonwebtoken.sign({ sub: user._id }, JWT_SECRET);
                body = { user, token };
            } else {
                status = 401;
                new Error('Unauthorized');
            }
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User auth',
                message: e.message || 'An error is occured into user auth',
            }
        }
        return res.status(status).json(body);
    }

}