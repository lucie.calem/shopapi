import Sac from '../models/Sac';
import Textile from '../models/Textile';
import fs from 'fs'; //file system

export default class ArticleController {

    //create article 
    static async store(req, res) {
        let status = 200;
        let body = {};

        try {
            let {typeArticle} = req.params;
            let article = typeArticle == 'textile'?
            await Textile.create(req.body) : typeArticle == 'sac'?
            await Sac.create(req.body): '';
            body = { article };
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Create Article',
                message: e.message || 'An error is occured into the Article creation',
            }
        }
        return res.status(status).json(body);
    }




    //get list of articles
    static async list(req, res) {
        let status = 200;
        let body = {};

        try {
            let {typeArticle} = req.params;
            let articles = typeArticle == 'textile'?
            await Textile.find().select('-__v') : typeArticle == 'sac'?
            await Sac.find().select('-__v'): '';


            body = { articles };
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'List of Articles',
                message: e.message || 'An error is occured into list of Articles',
            }
        }
        return res.status(status).json(body);
    }


    //get details of articles
    static async details(req, res) {
        let status = 200;
        let body = {};

        try {
            let { id } = req.params;
            let {typeArticle} = req.params;
            let articles = typeArticle == 'textile'?
            await Textile.findById(id).select('-__v') : typeArticle == 'sac'?
            await Sac.findById(id).select('-__v'): '';
            body = {articles};
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Details Articles',
                message: e.message || 'An error is occured into Details Articles',
            }
        }
        return res.status(status).json(body);
    }

    //upate one articles
    static async update(req, res) {
             
        let status = 200;
        let body = {};

        try {
            delete req.body.image;
            let {id,typeArticle} = req.params;
            
            // let article = await Sac.findByIdAndUpdate( id, req.body, { new: true}).populate('category')
            let article = typeArticle == 'textile'?
            await Textile.findByIdAndUpdate( id, req.body, { new: true}).populate('category') : typeArticle == 'sac'?
            await Sac.findByIdAndUpdate( id, req.body, { new: true}).populate('category'): '';
            body = {article}; 
           
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Update Articles',
                message: e.message || 'An error is occured into Update Articles',
            }
        }
        return res.status(status).json(body);
    }

    //delete one articles
    static async remove(req, res) {
        let status = 200;
        let body = {};

        try {
            let { id } = req.params;

            let {typeArticle} = req.params;
            let article = typeArticle == 'textile'?
            await Textile.findByIdAndDelete(id) : typeArticle == 'sac'?
            await Sac.findByIdAndDelete(id): '';
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Delete Articles',
                message: e.message || 'An error is occured into Delete Articles',
            }
        }
        return res.status(status).json(body);
    }

    static async updateImage(req,res){
        let status = 200;
        let body = {};

        try {
           let {typeArticle} = req.params;
            let article = typeArticle == 'textile'?
            await Textile.findById(req.params.id) : typeArticle == 'sac'?
            await Sac.findById(req.params.id): '';



           if(fs.existsSync(`./${article.image}`)){
               await fs.unlinkSync(`./${article.image}`);

           }
           

          article.image = req.body.image;
          await article.save();
          body={article};
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error ||  'Article update',
                message: e.message  || 'An error is occured into video remove',
            }
        }
        return res.status(status).json(body);


    }
}