import Category from '../models/Category';

export default class CategoryController {

    /**
     * create one category
     * 
     */
    static async store(req, res) {
        let status = 200;
        let body = {};

        try {
            let category = await Category.create(req.body);
            body = { category };
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Create category',
                message: e.message || 'An error is occured into the creation',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * List of categories
     */
    static async list(req, res) {
        let status = 200;
        let body = {};

        try {
            let categories = await Category.find().select('-__v')
            body = { categories };
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'List of categories',
                message: e.message || 'An error is occured into list of categories',
            }
        }
        return res.status(status).json(body);
    }

   /**
     * update one category
     * 
     */
    static async update(req, res) {
        let status = 200;
        let body = {};

        try {
            let { id } = req.params;
            let category = await Category.findByIdAndUpdate(id, req.body, { new: true}).select('-__v');
            body = {category};
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Update category',
                message: e.message || 'An error is occured into Update category',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * delete one category
     * 
     * 
     */
    static async remove(req, res) {
        let status = 200;
        let body = {};

        try {
            let { id } = req.params;
            let category = await Category.findByIdAndDelete(id);
        } catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Delete category',
                message: e.message || 'An error is occured into Delete category',
            }
        }
        return res.status(status).json(body);
    }

    
}