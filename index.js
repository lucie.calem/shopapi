import Server from "./src/Server";
import Database from "./src/Database";

const app = Server.config();
const {APP_PORT,DB_NAME,DB_HOST,DB_PORT} = process.env;

Database.init(`mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`)
.then(() => {

    console.log('la base de donnée est connecté au port 2707');

    app.listen(APP_PORT, () => {
        console.log(`le serveur est connecté sur le port ${APP_PORT}`); // `` langage js lecture de vars (template string)
    });//ecoute sur un port

});
export default app;